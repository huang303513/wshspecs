#
# Be sure to run `pod lib lint LibAdHub.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'LibAdHub'
  s.version          = '1.4.0'
  s.summary          = '这是一个实验的Pod库，我要用它做实验。呵呵呵'
  s.description      = '呵呵呵呵呵呵呵呵呵呵呵呵呵'
  s.homepage         = 'http://gitlab.com/huang303513/libadhub'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'huang303513' => 'huang2009303513@gmail.com' }
  s.source           = { :git => 'git@gitlab.com:huang303513/libadhub.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '8.0'
  s.source_files = 'LibAdHub/Classes/**/*'
  
  # # subspec, 将没有 Pod 版本的第三方库依赖做为 subspec.
  s.subspec 'libAdHub' do |ss|
    ss.vendored_libraries = 'LibAdHub/Vender/libAdHub/*.a'
    ss.vendored_frameworks = 'LibAdHub/Vender/libAdHub/*.framework'
    ss.resource_bundles = {'libAdHub' => [
        'LibAdHub/Vender/libAdHub/AdHubSDK.bundle',
        'LibAdHub/Vender/libAdHub/baidumobadsdk.bundle',
        'LibAdHub/Vender/libAdHub/FmobiBundle.bundle',
        'LibAdHub/Vender/libAdHub/WMAdSDK.bundle']
    }
    ss.frameworks = 'AVFoundation', 'CoreMedia', 'CoreTelephony', 'CoreLocation', 'CoreMotion', 'SystemConfiguration', 'AdSupport', 'CFNetwork', 'MessageUI', 'SafariServices', 'MobileCoreServices', 'Twitter', 'WebKit', 'StoreKit', 'CoreGraphics', 'JavaScriptCore'
    ss.libraries = 'xml2', 'iconv', 'z', 'sqlite3', 'c++'
  end
  
  s.dependency 'AFNetworking', '~> 3.1.0'
  s.dependency 'Masonry', '~> 1.1.0'

end
